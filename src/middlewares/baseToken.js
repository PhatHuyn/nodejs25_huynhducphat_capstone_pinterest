const jwt = require("jsonwebtoken");

// Ham tao Token
const parseToken = (data) => {
  let token = jwt.sign({ data }, "bimat", {
    algorithm: "HS256",
    expiresIn: "5y",
  });
  return token;
};

// Ham check token
const checktoken = (token) => {
  try {
    let checkT = jwt.verify(token, "bimat");
    if (checkT) {
      return { checkData: true, message: "" };
    }
  } catch (error) {
    return { checkData: false, message: error.message };
  }
};

const verifyToken = (req, res, next) => {
  const { token } = req.headers;
  const verifyToken = checktoken(token);
  if (verifyToken.checkData) {
    next();
  } else {
    res.status(401).send(verifyToken.message);
  }
};

module.exports = { parseToken, checktoken, verifyToken };
