const { sucessCode, errorCode, failCode } = require("../config/reponse");
const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const model = init_models(sequelize);

// Lấy thông tin bình luận theo id ảnh
const GetCommentIdImage = async (req, res) => {
  try {
    let { hinh_id } = req.params;

    let checkIdImage = await model.binh_luan.findOne({
      where: {
        hinh_id,
      },
    });

    if (checkIdImage) {
      let data = await model.binh_luan.findAll({
        where: {
          hinh_id,
        },
      });
      sucessCode(res, data, "Lấy dữ liệu bình luận thành công!!!");
    } else {
      failCode(
        res,
        checkIdImage,
        `Id ảnh ${hinh_id} này không có trong bình luận!!!`
      );
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Lưu thông tin bình luận của người dùng với hình ảnh
const SaveComment = async (req, res) => {
  try {
    let { nguoi_dung_id, hinh_id, noi_dung, ngay_binh_luan } = req.body;

    let data = await model.binh_luan.create({
      nguoi_dung_id,
      hinh_id,
      noi_dung,
      ngay_binh_luan,
    });
    sucessCode(res, data, "Lưu bình luận thành công!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

module.exports = { GetCommentIdImage, SaveComment };
