const { sucessCode, errorCode, failCode } = require("../config/reponse");
const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const model = init_models(sequelize);
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;

// Lấy danh sách ảnh
const getListImage = async (req, res) => {
  try {
    let data = await model.hinh_anh.findAll();
    sucessCode(res, data, "Danh sách ảnh!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Tìm kiếm danh sách ảnh theo tên
const SearchListImage = async (req, res) => {
  try {
    let { ten } = req.params;
    let data = await model.hinh_anh.findAll({
      where: {
        ten_hinh: {
          [Op.like]: `%${ten}%`,
        },
      },
    });
    sucessCode(res, data, "Dữ liệu tìm tên theo danh sách ảnh!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Lấy thông tin ảnh và người tạo ảnh bằng id ảnh
const GetListImageId = async (req, res) => {
  try {
    let { hinh_id } = req.params;

    let checkIdImage = await model.hinh_anh.findOne({
      where: {
        hinh_id,
      },
    });

    if (checkIdImage) {
      let data = await model.hinh_anh.findAll({
        include: ["nguoi_dung"],
        where: {
          hinh_id,
        },
      });
      sucessCode(res, data, "Lấy dữ liệu hình và người tạo thành công!!!");
    } else {
      failCode(res, checkIdImage, `Id ảnh ${hinh_id} này không tồn tại!!!`);
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Lấy danh sach anh da tạo theo user id
const GetIdUserAddImage = async (req, res) => {
  try {
    let { nguoi_dung_id } = req.params;

    let checkIdUser = await model.hinh_anh.findOne({
      where: {
        nguoi_dung_id,
      },
    });

    if (checkIdUser) {
      let data = await model.hinh_anh.findAll({
        where: {
          nguoi_dung_id,
        },
      });
      sucessCode(
        res,
        data,
        "Lấy dữ liệu ảnh đã tạo theo id user thành công!!!"
      );
    } else {
      failCode(
        res,
        checkIdUser,
        `Id User ${nguoi_dung_id} này chưa tạo ảnh!!!`
      );
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Xoá ảnh đã tạo theo id ảnh
const DeleteImage = async (req, res) => {
  try {
    let { hinh_id } = req.params;

    let checkIdImage = await model.hinh_anh.findOne({
      where: {
        hinh_id,
      },
    });

    if (checkIdImage) {
      let data = await model.hinh_anh.destroy({
        where: {
          hinh_id,
        },
      });
      sucessCode(res, data, "Xoá hình thành công!!!");
    } else {
      failCode(res, checkIdImage, `Id hình ${hinh_id} này chưa tạo!!!`);
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Thêm một ảnh của User
const AddImageUser = async (req, res) => {
  try {
    let { ten_hinh, duong_dan, mo_ta, nguoi_dung_id } = req.body;

    let data = await model.hinh_anh.create({
      ten_hinh,
      duong_dan,
      mo_ta,
      nguoi_dung_id,
    });
    sucessCode(res, data, "Thêm ảnh thành công!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

module.exports = {
  getListImage,
  SearchListImage,
  GetListImageId,
  GetIdUserAddImage,
  DeleteImage,
  AddImageUser,
};
