const { sucessCode, errorCode, failCode } = require("../config/reponse");
const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const model = init_models(sequelize);

const { parseToken } = require("../middlewares/baseToken");

// Đăng Ký(signUp)
const signUp = async (req, res) => {
  try {
    let { email, mat_khau, ho_ten, tuoi } = req.body;
    let checkEmail = await model.nguoi_dung.findOne({
      where: {
        email,
      },
    });

    if (checkEmail) {
      failCode(res, email, `Email đã tồn tại!!!`);
    } else {
      let data = await model.nguoi_dung.create({
        email,
        mat_khau,
        ho_ten,
        tuoi,
      });
      sucessCode(res, data, "Đăng ký thành công!!!");
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Đăng Nhập(Login)
const logIn = async (req, res) => {
  try {
    let { email, mat_khau } = req.body;
    let checkLogin = await model.nguoi_dung.findOne({
      where: {
        email,
      },
    });

    if (checkLogin) {
      if (checkLogin.mat_khau == mat_khau) {
        // sucessCode(res, checkLogin, "Đăng nhập thành công!!!");
        sucessCode(res, parseToken(checkLogin), "Đăng nhập thành công!!!");
      } else {
        failCode(res, "", `Mật khẩu không đúng!!!`);
      }
    } else {
      failCode(res, "", `Email không đúng!!!`);
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Láy thông tin User
const getListUser = async (req, res) => {
  try {
    let data = await model.nguoi_dung.findAll();
    sucessCode(res, data, "Thông tin User!!!");
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Cập nhật thông tin User
const UpdateUser = async (req, res) => {
  try {
    let { nguoi_dung_id } = req.params;
    let { email, mat_khau, ho_ten, tuoi, anh_dai_dien } = req.body;

    let checkIdUser = await model.nguoi_dung.findOne({
      where: {
        nguoi_dung_id,
      },
    });

    if (checkIdUser) {
      let data = await model.nguoi_dung.update(
        {
          email,
          mat_khau,
          ho_ten,
          tuoi,
          anh_dai_dien,
        },
        {
          where: {
            nguoi_dung_id,
          },
        }
      );
      sucessCode(res, data, "Cập nhật thông tin thành công!!!");
    } else {
      failCode(res, "", `Người dùng ${nguoi_dung_id} không tồn tại!!!`);
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

module.exports = { signUp, logIn, getListUser, UpdateUser };
