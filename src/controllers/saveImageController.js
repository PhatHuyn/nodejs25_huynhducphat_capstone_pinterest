const { sucessCode, errorCode, failCode } = require("../config/reponse");
const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const model = init_models(sequelize);

// Lấy thông tin luu anh theo id ảnh
const GetIdImageSave = async (req, res) => {
  try {
    let { hinh_id } = req.params;

    let checkIdImage = await model.luu_anh.findOne({
      where: {
        hinh_id,
      },
    });

    if (checkIdImage) {
      let data = await model.luu_anh.findAll({
        where: {
          hinh_id,
        },
      });
      sucessCode(res, data, "Lấy dữ liệu ảnh đã lưu theo id ảnh thành công!!!");
    } else {
      failCode(res, checkIdImage, `Id ảnh ${hinh_id} này chưa được lưu!!!`);
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

// Lấy danh sách ảnh đã lưu theo id user
const GetIdUserSaveImage = async (req, res) => {
  try {
    let { nguoi_dung_id } = req.params;

    let checkIdUser = await model.luu_anh.findOne({
      where: {
        nguoi_dung_id,
      },
    });

    if (checkIdUser) {
      let data = await model.luu_anh.findAll({
        where: {
          nguoi_dung_id,
        },
      });
      sucessCode(
        res,
        data,
        "Lấy dữ liệu ảnh đã lưu theo id user thành công!!!"
      );
    } else {
      failCode(
        res,
        checkIdUser,
        `Id User ${nguoi_dung_id} này chưa lưu ảnh!!!`
      );
    }
  } catch (err) {
    errorCode(res, "Lỗi Backend");
  }
};

module.exports = { GetIdImageSave, GetIdUserSaveImage };
