const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("db_capstone_pinterest", "root", "1234", {
  host: "localhost",
  dialect: "mysql",
  port: 3306,
});

// Test kết nối mySQL
// try {
//   sequelize.authenticate();
//   console.log("Thành công");
// } catch {
//   console.log("thất bại");
// }

module.exports = sequelize;
