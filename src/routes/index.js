const express = require("express");

const rootRoute = express.Router();
const userRoute = require("./userRoutes");
const imageRoute = require("./imageRoutes");
const commentRoute = require("./commentRoutes");
const saveImageRoute = require("./saveImageRoutes");

rootRoute.use("/user", userRoute);

rootRoute.use("/image", imageRoute);

rootRoute.use("/comment", commentRoute);

rootRoute.use("/saveImage", saveImageRoute);

module.exports = rootRoute;
