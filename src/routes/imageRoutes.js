const express = require("express");
const imageRoute = express.Router();
const {
  getListImage,
  SearchListImage,
  GetListImageId,
  GetIdUserAddImage,
  DeleteImage,
  AddImageUser,
} = require("../controllers/imageController");
const { verifyToken } = require("../middlewares/baseToken");

// Lấy danh sách ảnh
imageRoute.get("/getListImage", verifyToken, getListImage);

// Tìm kiếm danh sách ảnh theo tên
imageRoute.get("/searchNameImage/:ten", verifyToken, SearchListImage);

// Lấy thông tin ảnh và người tạo ảnh bằng id ảnh
imageRoute.get("/getlistImageId/:hinh_id", verifyToken, GetListImageId);

// Lấy danh sach anh da tạo theo user id
imageRoute.get(
  "/getIdUserAddImage/:nguoi_dung_id",
  verifyToken,
  GetIdUserAddImage
);

// Xoá ảnh đã tạo theo id ảnh
imageRoute.delete("/deleteImage/:hinh_id", verifyToken, DeleteImage);

// Thêm một ảnh của User
imageRoute.post("/addImage", verifyToken, AddImageUser);

module.exports = imageRoute;
