const express = require("express");
const userRoute = express.Router();
const {
  signUp,
  logIn,
  getListUser,
  UpdateUser,
} = require("../controllers/userController");

const { verifyToken } = require("../middlewares/baseToken");

// Đăng Ký(signUp)
userRoute.post("/signup", signUp);

//Đăng Nhập(Login)
userRoute.get("/login", logIn);

// Lấy thông tin User
userRoute.get("/getUser", verifyToken, getListUser);

// Cập nhật thông tin User
userRoute.put("/UpdateUser/:nguoi_dung_id", verifyToken, UpdateUser);

module.exports = userRoute;
