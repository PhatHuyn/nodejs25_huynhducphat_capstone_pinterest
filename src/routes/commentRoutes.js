const express = require("express");
const commentRoute = express.Router();
const {
  GetCommentIdImage,
  SaveComment,
} = require("../controllers/commentController");
const { verifyToken } = require("../middlewares/baseToken");

// Lấy thông tin bình luận theo id ảnh
commentRoute.get("/getCommentIdImage/:hinh_id", verifyToken, GetCommentIdImage);

// Lưu thông tin bình luận của người dùng với hình ảnh
commentRoute.post("/saveComment", verifyToken, SaveComment);

module.exports = commentRoute;
