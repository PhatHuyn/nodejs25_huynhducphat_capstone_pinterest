const express = require("express");
const saveImageRoute = express.Router();
const {
  GetIdImageSave,
  GetIdUserSaveImage,
} = require("../controllers/saveImageController");
const { verifyToken } = require("../middlewares/baseToken");

// Lấy thông tin luu anh theo id ảnh
saveImageRoute.get("/getImageSave/:hinh_id", verifyToken, GetIdImageSave);

// Lấy danh sách ảnh đã lưu theo id user
saveImageRoute.get(
  "/getIdUserSaveImage/:nguoi_dung_id",
  verifyToken,
  GetIdUserSaveImage
);

module.exports = saveImageRoute;
